- 👋 Hi, I’m @code-victor-now
- 👀 I’m interested in starting a career as a software developer.
- 🌱 I’m currently learning the basics of Python.
- 💞️ I’m looking to collaborate on small projects, because I´m still learning. 
- 📫 You can get in contact with me by email: vcalesco@gmail.com

<!---
Calesco/Calesco is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
